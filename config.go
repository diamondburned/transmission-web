package main

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"strconv"
	"strings"

	"github.com/odwrtw/transmission"
)

// Config is the config
type Config struct {
	Host         string       `json:"host"`
	Transmission Transmission `json:"transmission"`
}

// DefaultConfig is to generate a default config
func defaultConfig() *Config {
	return &Config{
		Host: "0.0.0.0:5000",
		Transmission: Transmission{
			Address:  "localhost",
			Port:     9091,
			Path:     "/transmission/rpc",
			Username: "transmission",
			Password: "transmission",
			HTTPS:    false,
		},
	}
}

// newConfig loads the entire config file and do tests
func newConfig(path string) (*Config, error) {
	var config = defaultConfig()
	if err := config.read(path); err != nil {
		return nil, err
	}

	// TODO(ym): Move this somewhere else.
	if config.Transmission.Address == "" {
		return nil, errors.New("Transmission not configured")
	}

	if strings.HasPrefix(config.Transmission.Password, "b64|") {
		decoded, err := base64.StdEncoding.DecodeString(
			strings.Replace(config.Transmission.Password, "b64|", "", 1),
		)

		if err != nil {
			return nil, fmt.Errorf(
				"Failed to convert base64 %s to string",
				config.Transmission.Password,
			)
		}

		config.Transmission.Password = string(decoded)
	}

	return config, nil
}

// read reads the config file
func (config *Config) read(path string) error {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}
	err = json.Unmarshal(data, &config)
	if err != nil {
		return err
	}
	return nil
}

// Write writes the config file
func (config *Config) write(path string) error {
	json, err := json.MarshalIndent(config, "", "\t")
	if err != nil {
		return err
	}
	if err := ioutil.WriteFile(path, json, os.ModePerm); err != nil {
		return err
	}
	return nil
}

func parseCfg(path string) *Config {
	// var usr, _ = user.Current()
	var configFile, err = newConfig(path)
	if err != nil {
		if os.Rename("./config.json", "./config.json.bak") != nil {
			log.Println("Couldn't move config.json to config.json.bak. Proceeding to override.")
		}

		if err := defaultConfig().write(path); err != nil {
			log.Fatalln(err)
		}

		os.Exit(1)
	}

	return configFile
}

// Transmission has the information for RPC
type Transmission struct {
	Address  string
	Port     uint16
	Path     string
	Username string
	Password string
	HTTPS    bool
}

func (tr Transmission) initConnection() *transmission.Client {
	var url = url.URL{
		Scheme: "http",
		Host:   tr.Address + ":" + strconv.Itoa(int(tr.Port)),
		Path:   tr.Path,
	}

	if tr.HTTPS {
		url.Scheme = "https"
	}

	log.Println("RPC URL:", url.String())

	conn, err := transmission.New(transmission.Config{
		Address:  url.String(),
		User:     tr.Username,
		Password: tr.Password,
	})

	if err != nil {
		log.Fatalln("Connection failed:", err)
	}

	return conn
}
