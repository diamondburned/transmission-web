package main

import (
	"flag"
	"html/template"
	"log"
	"net/http"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/gobuffalo/packr/v2"
)

var (
	cfgPath string

	public = packr.New("public", "./public")

	loginTpl    *template.Template
	indexTpl    *template.Template
	statsTpl    *template.Template
	torrentsTpl *template.Template
)

// MustBox because error handing is a pain
func MustBox(asset string) string {
	s, e := public.FindString(asset)
	if e != nil {
		log.Fatalln("Failed to find asset:", e)
	}

	return s
}

func main() {
	flag.StringVar(&cfgPath, "path", "config.json", "The path to the config")
	flag.Parse()

	log.SetFlags(log.Lshortfile)

	var (
		login    = MustBox("login.html")
		index    = MustBox("index.html")
		stats    = MustBox("stats.html")
		torrents = MustBox("torrents.html")
	)

	loginTpl = template.Must(template.New("loginTpl").Parse(login))
	indexTpl = template.Must(template.New("indexTpl").Parse(index))
	statsTpl = template.Must(template.New("statsTpl").Parse(stats))
	torrentsTpl = template.Must(template.New("torrentsTpl").Parse(torrents))

	cfg := parseCfg(cfgPath)
	client := cfg.Transmission.initConnection()

	state := NewState(client)
	auth := NewAuthenticateState()

	mux := chi.NewMux()
	mux.Use(middleware.Recoverer)
	mux.Use(middleware.Compress(5))

	mux.HandleFunc("/login", auth.LoginHandler(cfg))

	// Authenticated group.
	mux.Group(func(mux chi.Router) {
		mux.Use(auth.Middleware)
		mux.Use(state.InjectHTTP)

		// Cache-free group.
		mux.Group(func(mux chi.Router) {
			mux.Use(middleware.NoCache)

			mux.Mount("/torrents", http.HandlerFunc(LoadTorrents))
			mux.Mount("/stats", http.HandlerFunc(LoadStats))
			mux.Mount("/upload", http.HandlerFunc(UploadTorrent))
			mux.Mount("/api/{command}", http.HandlerFunc(APIHandler))
		})

		mux.Mount("/static", http.HandlerFunc(StaticHandler))
		mux.Get("/", HomeHandler)
	})

	go func() {
		// Keep the cache at latest 10 seconds.
		for range time.Tick(10 * time.Second) {
			state.AskUpdate(false)
		}
	}()

	if err := http.ListenAndServe(cfg.Host, mux); err != nil {
		log.Fatalln("Failed to listen and serve HTTP:", err)
	}
}

// Render a template, or server error.
func render(
	w http.ResponseWriter, r *http.Request,
	tpl *template.Template, name string, data interface{}) {

	w.Header().Set("Content-Type", "text/html; charset=utf-8")

	if err := tpl.ExecuteTemplate(w, name, data); err != nil {
		log.Println("\nRender Error:", err)
		return
	}
}
