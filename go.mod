module gitlab.com/diamondburned/transmission-web

go 1.14

require (
	github.com/dustin/go-humanize v1.0.0
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/gobuffalo/packr/v2 v2.8.0
	github.com/karrick/godirwalk v1.15.6 // indirect
	github.com/odwrtw/transmission v0.0.0-20200601100646-f8e794395179
	github.com/pkg/errors v0.8.0
	github.com/rogpeppe/go-internal v1.6.0 // indirect
	github.com/satori/go.uuid v1.2.1-0.20181028125025-b2ce2384e17b
	github.com/sirupsen/logrus v1.6.0 // indirect
	golang.org/x/crypto v0.0.0-20200510223506-06a226fb4e37 // indirect
	golang.org/x/sys v0.0.0-20200523222454-059865788121 // indirect
)
