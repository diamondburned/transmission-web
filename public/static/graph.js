window.speedSeries = [
	[0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0]
]

window.chart = new Chartist.Line('#chart', {
	series: window.speedSeries,
}, {
	// width: '220px',
	// height: '165px',
	showPoint: false,
	showLine: true,
	showArea: true,
	fullWidth: true,
	showLabel: false,
	lineSmooth: Chartist.Interpolation.cardinal({
		tension: 0.2
	}),
	axisX: {
		showGrid: false,
		showLabel: false,
		offset: 0,
	},
	axisY: {
		showGrid: false,
		showLabel: false,
		offset: 0
	},
	chartPadding: {
		top: 12,
		right: 0,
		bottom: 0,
		left: 0
	},
	low: 0,
	areaBase: 0,
	onlyInteger: true,
})
