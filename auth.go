package main

import (
	"errors"
	"net/http"
	"net/url"
	"sync"
	"time"

	uuid "github.com/satori/go.uuid"
)

var (
	// TokenLifeSpan is the time before a token is seen as expired
	TokenLifeSpan = time.Duration(time.Hour * 12)

	// ErrExpired is returned when the token is expired
	ErrExpired = errors.New("Token expired")

	// ErrUnauthorized is returned when the user is not in the list
	ErrUnauthorized = errors.New("Unauthorized")
)

// IncorrectType is used in the URL form during redirection and
// in templating of the GET page to indicate what's wrong with
// the login
type IncorrectType string

const (
	// IncorrectUsername is returned when the form has an incorrect username
	// Unknown is used for anonimity
	IncorrectUsername = "Unknown user"

	// IncorrectPassword is returned when the form has an incorrect password
	IncorrectPassword = "Incorrect password"
)

// MakeIncorrectForm generates a URL form with the proper incorrect type
func MakeIncorrectForm(itype IncorrectType) string {
	f := url.Values{}
	f.Add("incorrect", string(itype))
	return f.Encode()
}

// ReflectIncorrectForm parses the request for incorrect type
func ReflectIncorrectForm(r *http.Request) IncorrectType {
	switch f := r.FormValue("incorrect"); f {
	case IncorrectPassword:
		return IncorrectPassword
	case IncorrectUsername:
		return IncorrectUsername
	default:
		return ""
	}
}

type AuthenticateState struct {
	mutex sync.Mutex

	// sessions contains an array of authenticated tokens time.Time is the
	// authenticated time
	sessions map[string]time.Time
}

func NewAuthenticateState() *AuthenticateState {
	return &AuthenticateState{
		sessions: make(map[string]time.Time),
	}
}

func (s *AuthenticateState) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if err := s.Query(r); err != nil {
			http.Redirect(w, r, "/login", http.StatusFound)
			return
		}

		next.ServeHTTP(w, r)
	})
}

// Query checks cookie for token and expiry
func (s *AuthenticateState) Query(r *http.Request) error {
	t, err := r.Cookie("token")
	if err != nil {
		if err == http.ErrNoCookie {
			return ErrUnauthorized
		}

		return err
	}

	s.mutex.Lock()
	defer s.mutex.Unlock()

	for tkn, exp := range s.sessions {
		if tkn == t.Value {
			if exp.Sub(time.Now()) < 0 {
				return ErrExpired
			}

			return nil
		}
	}

	return ErrUnauthorized
}

// LoginHandler handles login GET+POST
func (s *AuthenticateState) LoginHandler(cfg *Config) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case "GET":
			d := map[string]interface{}{
				"Message": ReflectIncorrectForm(r),
			}

			render(w, r, loginTpl, "loginTpl", d)

		case "POST":
			var (
				username = r.FormValue("username")
				password = r.FormValue("password")
			)

			if username != cfg.Transmission.Username {
				http.Redirect(
					w, r,
					"/login?"+MakeIncorrectForm(IncorrectPassword),
					http.StatusFound,
				)

				return
			}

			if password != cfg.Transmission.Password {
				http.Redirect(
					w, r,
					"/login?"+MakeIncorrectForm(IncorrectPassword),
					http.StatusFound,
				)

				return
			}

			id, err := uuid.NewV4()
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			expires := time.Now().Add(TokenLifeSpan)

			http.SetCookie(w, &http.Cookie{
				Name:     "token",
				Value:    id.String(),
				Expires:  expires,
				SameSite: http.SameSiteStrictMode,
			})

			s.mutex.Lock()
			s.sessions[id.String()] = expires
			s.mutex.Unlock()

			http.Redirect(w, r, "/", http.StatusFound)

		default:
			http.Error(w, "GET/POST only", http.StatusNotFound)
		}
	}
}
