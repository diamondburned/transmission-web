# transmission-web

This is a WIP project. Nothing works yet (other than just printing torrents).
 
# Quick documentation

- The URL takes in 2 options:
    - `?sort=[SortMode]` - Sorts the torrent list
    - `?reversed=[int]` - `1` is `true`, `0` is `false`

# References

## `SortMode`

```go
const (
	// AddedDate sorts the date added in ascending order
	AddedDate SortMode = 2 // AddedDate
	// ActivityDate sorts the activity date in ascending order
	ActivityDate SortMode = 3 // ActivityDate
	// DoneDate sorts the date finished in ascending order
	DoneDate SortMode = 4 // DoneDate
	// Name sorts the names alphabetically (A-Z)
	Name SortMode = 5 // Name
	// Size sorts the filesize incrementally
	Size SortMode = 6 // TotalSize
	// Ratio sorts the ratios decrementally (?)
	Ratio SortMode = 7
	// UploadSpeed sorts the upload speed, largest first
	UploadSpeed SortMode = 8
	// DownloadSpeed sorts the download speed, largest first
	DownloadSpeed SortMode = 9
)
```

# What I got + todo

- Selection is done. Next is to store the ID inside a hidden element and use that for the ID to delete. 
- Should also do Shift + Click - to erase data

# Progress so far

![Progress as of November 18](http://u.cubeupload.com/diamondburned/dYSHvH.png)

# Todo

- [ ] Better static assets packing
- [ ] ~~Move to `fasthttprouter`~~ This is dumb and pointless.
