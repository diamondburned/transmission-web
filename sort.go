package main

// SortMode is the code for methods of sorting
type SortMode int

const (
	// ID sorts IDs in ascending order !!! DEPRECATED
	ID SortMode = 1 // ID
	// AddedDate sorts the date added in ascending order
	AddedDate SortMode = 2 // AddedDate
	// ActivityDate sorts the activity date in ascending order
	ActivityDate SortMode = 3 // ActivityDate
	// DoneDate sorts the date finished in ascending order
	DoneDate SortMode = 4 // DoneDate
	// Name sorts the names alphabetically (A-Z)
	Name SortMode = 5 // Name
	// Size sorts the filesize incrementally
	Size SortMode = 6 // TotalSize
	// Ratio sorts the ratios decrementally (?)
	Ratio SortMode = 7
	// UploadSpeed sorts the upload speed, largest first
	UploadSpeed SortMode = 8
	// DownloadSpeed sorts the download speed, largest first
	DownloadSpeed SortMode = 9
)

// TorrentsSortable is the sort-er
type TorrentsSortable struct {
	Torrents []HTMLTorrent
	Mode     SortMode
}

func (ss TorrentsSortable) Len() int {
	return len(ss.Torrents)
}

func (ss TorrentsSortable) Swap(i, j int) {
	ss.Torrents[i], ss.Torrents[j] = ss.Torrents[j], ss.Torrents[i]
}

func (ss TorrentsSortable) Less(i, j int) bool {
	switch ss.Mode {
	case AddedDate:
		break
	case ActivityDate:
		return ss.Torrents[i].torrent.ActivityDate > ss.Torrents[j].torrent.ActivityDate
	case DoneDate:
		return ss.Torrents[i].torrent.DoneDate > ss.Torrents[j].torrent.DoneDate
	case Name:
		return ss.Torrents[i].torrent.Name < ss.Torrents[j].torrent.Name
	case Size:
		return ss.Torrents[i].torrent.TotalSize > ss.Torrents[j].torrent.TotalSize
	case Ratio:
		return ss.Torrents[i].torrent.UploadRatio > ss.Torrents[j].torrent.UploadRatio
	case UploadSpeed:
		return ss.Torrents[i].torrent.RateUpload > ss.Torrents[j].torrent.RateUpload
	case DownloadSpeed:
		return ss.Torrents[i].torrent.RateDownload > ss.Torrents[j].torrent.RateDownload
	}

	// Default is AddedDate
	return ss.Torrents[i].torrent.AddedDate > ss.Torrents[j].torrent.AddedDate
}
