new Chartist.Line('#chart', {
	series: [
		[5, 2, 4, 2, 0], // a, download
		[1, 3, 2, 7, 3], // b, upload
	]
}, {
	// width: '220px',
	// height: '165px',
	showPoint: false,
	showLine: true,
	showArea: true,
	fullWidth: true,
	showLabel: false,
	axisX: {
		showGrid: false,
		showLabel: false,
		offset: 0
	},
	axisY: {
		showGrid: false,
		showLabel: false,
		offset: 0
	},
	chartPadding: {
		top: 5,
		right: 0,
		bottom: 0,
		left: 0
	},
	low: 0
})