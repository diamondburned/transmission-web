var DefaultSettings = {
	"updateTorrents": 1500,
	"updateStats": 3000
}

function callUpload() {
	document.getElementById("upload").style.display = "block";
}

function hideUpload() {
	document.getElementById("upload").style.display = "none"
}

function mkDialog(msg) {
	document.getElementById("dialog-bg").style.display = "block"

	if (msg instanceof Object) {
		let d = document.getElementById("dialog-message")
		d.innerHTML = ""
		d.appendChild(msg)
	} else {
		if (msg) {
			document.getElementById("dialog-message").innerHTML = msg
		} else {
			document.getElementById("dialog-bg").style.display = "none"
		}
	}
}

async function init() {
	const s = window.location.search
	updateTorrents(s)
	updateStats()

	window.upload = document.getElementById("upload-label");

	['dragenter', 'dragover', 'dragleave', 'drop'].forEach(function (eventName) {
		window.upload.addEventListener(eventName, function (e) {
			e.preventDefault()
			e.stopPropagation()
		}, false)
	});

	['dragenter', 'dragover'].forEach(function (eventName) {
		window.upload.addEventListener(eventName, function (e) {
			window.upload.classList.add('highlight')
		}, false)
	});

	['dragleave', 'drop'].forEach(function (eventName) {
		window.upload.addEventListener(eventName, function (e) {
			window.upload.classList.remove('highlight')
		}, false)
	});

	['drop'].forEach(function (eventName) {
		window.upload.addEventListener(eventName, function (e) {
			handleFiles(e.dataTransfer.files)
			hideUpload()
		}, false)
	});

	['drop'].forEach(function (eventName) {
		window.upload.addEventListener(eventName, function (e) {
			handleFiles(e.dataTransfer.files)
			hideUpload()
		}, false)
	});

	document.getElementById("magnet-link").addEventListener(
		"keyup",
		function (e) {
			e.preventDefault()
			if (e.keyCode == 13) {
				uploadMagnet(document.getElementById("magnet-link").value)
			}
		}
	)

	document.getElementById("remove").addEventListener(
		"click",
		function (e) {
			e.preventDefault()
			callRemove(e)
		}
	)
}

async function handleFiles(files) {
	var uploaded = `<p class="title">Finished uploading</p>\n<p class="body">\n`;

	for (let file of files) {
		let err = await uploadFile(file)
		if (err !== false) {
			console.log(err)
			json = JSON.parse(err)
			mkDialog(`Failed at ${file.name}, reason:\n\t${json.message}`)
			return
		} else {
			uploaded += `${file.name}\n`
		}
	}

	console.log(uploaded)
	mkDialog("\n</p>" + uploaded)
}

async function uploadFile(file) {
	console.log("Uploading", file.name)

	let formData = new FormData()
	formData.append('uploadfile', file)

	let e = await fetch("/upload", {
		method: 'POST',
		body: formData
	})

	if (e.status !== 200) {
		let text = await e.text()
		return text
	}

	console.log("Success", e)
	return false
}


async function updateTorrents(s) {
	if (document.hidden) {
		return
	}

	let response = await fetch(`/torrents${s}`)
	if (response.status !== 200) {
		return
	}

	response.text().then(function (text) {
		document.getElementById("torrents").innerHTML = text
	})
}

async function updateStats() {
	if (document.hidden) {
		return
	}
	let response = await fetch(`/stats`)
	if (response.status !== 200) {
		return
	}



	response.text().then(function (text) {
		document.getElementById("stats").innerHTML = text
	})
}

function callRemove(e) {
	e.target.blur()
	console.log(document.activeElement)
}

async function torrentSettings(ev) {
	let torrentDiv = ev.target.parentElement.parentElement

	let actionsDiv = mkElemClass("div", "dialog actions actions")

	let titleInfo = mkElemClass("p", "dialog title")
	/**/
	titleInfo.innerText = "Properties"

	let info = mkElemClass("p", "dialog info")

	let infoLines = new Array()
	/**/
	infoLines.push("ID: " + torrentDiv.id)

	torrentDiv.childNodes.forEach(node => {
		if (node.tagName === "P")
			infoLines.push("Name: " + node.innerText)
	})

	let e = await fetch("/api/torrent?id=" + torrentDiv.id)

	let text = await e.text()
	let json = JSON.parse(text)

	if (e.status !== 200) {
		console.log(text)

		mkDialog(`Failed sending ${send}, reason:\n\t${json.message}`)
		return
	}

	infoLines.push("Peers: " + json.Peers)
	infoLines.push("Trackers: " + json.Trackers)
	infoLines.push("Activity Date: " + json.ActivityDate)
	infoLines.push("Added Date: " + json.AddedDate)
	infoLines.push("Location: " + json.DownloadDir)
	infoLines.push("Comment: " + json.Comment)

	info.innerText = infoLines.join("\n")

	actionsDiv.appendChild(titleInfo)
	actionsDiv.appendChild(info)

	let titleActions = mkElemClass("p", "dialog actions title")
	/**/
	titleActions.innerText = "Actions"

	actionsDiv.appendChild(titleActions)

	let style = window.getComputedStyle(torrentDiv)

	let Btndiv = mkElemClass("div", "dialog actions button")

	let pauseBtn = mkElemClass("p", "dialog actions pause")

	if (style.opacity == 1) {
		pauseBtn.innerHTML = "Pause"
		pauseBtn.onclick = function () {
			postToAPI("/api/pause", torrentDiv.id)
		}
	} else {
		pauseBtn.innerHTML = "Continue"
		pauseBtn.onclick = function () {
			postToAPI("/api/continue", torrentDiv.id)
		}
	}

	let removeBtn = mkElemClass("p", "dialog actions remove")
	/**/
	removeBtn.innerHTML = "Remove"

	let deleteBtn = mkElemClass("p", "dialog actions delete")
	/**/
	deleteBtn.innerHTML = "Delete data"

	Btndiv.appendChild(pauseBtn)
	Btndiv.appendChild(removeBtn)
	Btndiv.appendChild(deleteBtn)


	actionsDiv.appendChild(Btndiv)

	mkDialog(actionsDiv)
}

function mkElemClass(elem, cls) {
	let element = document.createElement(elem)
	element.setAttribute("class", cls)

	return element
}

async function postToAPI(path, ids) {
	let send = ""
	switch (typeof ids) {
		case "object":
			send = ids.join(",")
		default:
			send = [ids].join(",")
	}

	let formData = await new FormData()
	formData.append('ids', send)

	let e = await fetch(path, {
		method: 'POST',
		body: formData
	})

	if (e.status !== 200) {
		let text = await e.text()
		console.log(text)
		let json = JSON.parse(text)

		mkDialog(`Failed sending ${send}, reason:\n\t${json.message}`)
		return
	}

	mkDialog()
	updateTorrents(window.location.search)
}

// setSettings is a wrapper around getItem, 
// properly returning default settings when there's none stored
function getSettings(func_name) {
	let st = window.localStorage.getItem(func_name)
	if (st === null) {
		for (key in DefaultSettings) {
			if (func_name == key) {
				return DefaultSettings[key]
			}
		}
	}

	return st
}

// setSettings is a wrapper around SetItem, 
// with proper default settings
function setSettings(func_name, s) {
	if (!s) {
		for (key in DefaultSettings) {
			if (func_name == key) {
				return window.localStorage.setItem(key, DefaultSettings[key])
			}
		}
	} else {
		window.localStorage.setItem(func_name, s)
	}
}