new Chartist.Line('#chart', {
	series: [
		[5, 2, 4, 2, 0],
		[1, 3, 2, 7, 3],
	]
}, {
	// width: '220px',
	// height: '165px',
	showPoint: false,
	showLine: true,
	showArea: true,
	fullWidth: true,
	showLabel: false,
	axisX: {
		showGrid: false,
		showLabel: false,
		offset: 0
	},
	axisY: {
		showGrid: false,
		showLabel: false,
		offset: 0
	},
	chartPadding: 0,
	low: 0
})