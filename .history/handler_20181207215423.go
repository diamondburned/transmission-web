package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/hekmon/transmissionrpc"
)

type Response struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Time    int64  `json:"epoch_time"`
}

type TorrentInfo struct {
	ActivityDate string
	AddedDate    string
	Comment      string
	Peers        string
	DownloadDir  string
	Trackers     string
	Time         int64
}

// HomeHandler renders the homepage view template
func HomeHandler(w http.ResponseWriter, r *http.Request) {
	push(w, "/public/style.css")
	push(w, "/public/script.js")
	w.Header().Set("Content-Type", "text/html; charset=utf-8")

	render(w, r, indexTpl, "indexTpl", nil)
}

// LoadTorrents is separated for AJAX purposes
// Contains a list of torrents and stuff
func LoadTorrents(w http.ResponseWriter, r *http.Request) {
	sortmode := AddedDate
	switch r.URL.Query().Get("sort") {
	case "ID":
		sortmode = ID
	case "AddedDate":
		sortmode = AddedDate
	case "ActivityDate":
		sortmode = ActivityDate
	case "DoneDate":
		sortmode = DoneDate
	case "Name":
		sortmode = Name
	case "Size":
		sortmode = Size
	case "Ratio":
		sortmode = Ratio
	case "UploadSpeed":
		sortmode = UploadSpeed
	case "DownloadSpeed":
		sortmode = DownloadSpeed
	}

	reversed := false
	switch r.URL.Query().Get("reversed") {
	case "1":
		reversed = true
	}

	torrents, err := getTorrents(sortmode, reversed)
	if err != nil {
		response, JSON := craftResponse(500, err.Error())
		w.WriteHeader(response.Code)
		w.Write(JSON)
		return
	}

	fullData := map[string]interface{}{
		"Torrents": torrents,
	}

	render(w, r, torrentsTpl, "torrentsTpl", fullData)
}

// LoadStats is separated for AJAX purposes
// Contains stats for download speed, upload speed, etc
func LoadStats(w http.ResponseWriter, r *http.Request) {
	stats, err := getStats()
	if err != nil {
		response, JSON := craftResponse(500, err.Error())
		w.WriteHeader(response.Code)
		w.Write(JSON)
		return
	}

	fullData := map[string]interface{}{
		"Stats": stats,
	}

	render(w, r, statsTpl, "statsTpl", fullData)
}

// APIHandler does API stuff
func APIHandler(w http.ResponseWriter, r *http.Request) {
	URL := r.URL.Path
	log.Println(URL, strings.Split(URL, "/")[2])

	switch strings.Split(URL, "/")[2] {
	// /api/pause and /api/continue are the endpoint for pausing torrent(s)
	// Usage: Send a form request to /api/[pause|continue], name "ids", torrent IDs separated by a comma
	// Example: "23,10,6,3"
	case "pause", "continue":
		if r.Method != "POST" {
			response, JSON := craftResponse(400, "Endpoint only accepts POST request.")
			w.WriteHeader(response.Code)
			w.Write(JSON)
			return
		}

		form := r.FormValue("ids")
		if form == "" {
			response, JSON := craftResponse(400, "Missing ID. ID should look like: '1,2,3,5'...")
			w.WriteHeader(response.Code)
			w.Write(JSON)
			return
		}

		ids := strings.Split(form, ",")
		IDs := []int64{}

		for _, id := range ids {
			ID, err := strconv.Atoi(id)
			if err != nil {
				response, JSON := craftResponse(500, err.Error())
				w.WriteHeader(response.Code)
				w.Write(JSON)
				return
			}

			log.Println("Found", ID)

			IDs = append(IDs, int64(ID))
		}

		var err error

		switch strings.Split(URL, "/")[2] {
		case "pause":
			err = conn.TorrentStopIDs(IDs)
		case "continue":
			err = conn.TorrentStartIDs(IDs)
		}

		if err != nil {
			response, JSON := craftResponse(500, err.Error())
			w.WriteHeader(response.Code)
			w.Write(JSON)
		} else {
			response, JSON := craftResponse(200, "Successfully set for torrents "+form)
			w.WriteHeader(response.Code)
			w.Write(JSON)
		}
	case "torrent":
		id := r.FormValue("id")
		if id == "" {
			response, JSON := craftResponse(400, "Missing ID.")
			w.WriteHeader(response.Code)
			w.Write(JSON)
			return
		}

		ID, err := strconv.Atoi(id)
		if err != nil {
			response, JSON := craftResponse(500, err.Error())
			w.WriteHeader(response.Code)
			w.Write(JSON)
			return
		}

		fields := []string{
			"activityDate",
			"addedDate",
			"comment",
			"peers",
			"peersConnected",
			"downloadDir",
		}

		torrent, err := conn.TorrentGet(fields, []int64{int64(ID)})

		torrentInfo := TorrentInfo{
			ActivityDate: torrent[0].ActivityDate.Format(time.RFC1123Z),
			AddedDate:    torrent[0].AddedDate.Format(time.RFC1123Z),
			Comment:      *torrent[0].Comment,
			Peers:        fmt.Sprintf("%d / %d", *torrent[0].PeersConnected, len(torrent[0].Peers)),
			Trackers:     strconv.Itoa(len(torrent[0].Trackers)),
			DownloadDir:  *torrent[0].DownloadDir,
			Time:         time.Now().Unix(),
		}

		bytes, err := json.Marshal(torrentInfo)

		w.WriteHeader(200)
		w.Write(bytes)
	default:
		response, JSON := craftResponse(404, "API path not found")
		w.WriteHeader(response.Code)
		w.Write(JSON)
	}
}

// UploadTorrent listens to a XHRHTTPRequest containing a torrent binary and adds it
func UploadTorrent(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		response, JSON := craftResponse(400, "Upload only accepts POST request.")
		w.WriteHeader(response.Code)
		w.Write(JSON)
		return
	}

	r.ParseMultipartForm(32 << 20)
	headers := r.MultipartForm.File["uploadfile"]
	if headers == nil {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			response, JSON := craftResponse(500, err.Error())
			w.WriteHeader(response.Code)
			w.Write(JSON)
			return
		}

		if len(body) == 0 {
			response, JSON := craftResponse(400, "Body is empty")
			w.WriteHeader(response.Code)
			w.Write(JSON)
			return
		}

		magnet := string(body)
		if !strings.HasPrefix(magnet, "magnet:?") {
			response, JSON := craftResponse(400, "Not a magnet link")
			w.WriteHeader(response.Code)
			w.Write(JSON)
			return
		}

		torrent, err := conn.TorrentAdd(&transmissionrpc.TorrentAddPayload{Filename: &magnet})
		if err != nil {
			response, JSON := craftResponse(500, err.Error())
			w.WriteHeader(response.Code)
			w.Write(JSON)
			return
		}

		response, JSON := craftResponse(200, fmt.Sprintf("Added torrent %s", *torrent.Name))
		w.WriteHeader(response.Code)
		w.Write(JSON)

		return
	}

	if len(headers) == 0 {
		response, JSON := craftResponse(400, "No files in form 'uploadfile'")
		w.WriteHeader(response.Code)
		w.Write(JSON)
		return
	}

	// If the request does indeed contain files

	for _, header := range headers {
		file, err := header.Open()
		if err != nil {
			response, JSON := craftResponse(500, err.Error())
			w.WriteHeader(response.Code)
			w.Write(JSON)
			return
		}

		log.Println("Received file:", header.Filename, "/ size:", header.Size)

		bytes, err := ioutil.ReadAll(file)
		if err != nil {
			response, JSON := craftResponse(500, err.Error())
			w.WriteHeader(response.Code)
			w.Write(JSON)
			return
		}

		file.Close()

		if len(bytes) < 2 {
			response, JSON := craftResponse(400, "Missing data-binary as a torrent file.")
			w.WriteHeader(response.Code)
			w.Write(JSON)
			return
		}

		b64 := base64.StdEncoding.EncodeToString(bytes)

		torrent, err := conn.TorrentAdd(&transmissionrpc.TorrentAddPayload{MetaInfo: &b64})
		if err != nil {
			response, JSON := craftResponse(500, err.Error())
			w.WriteHeader(response.Code)
			w.Write(JSON)
			return
		}

		response, JSON := craftResponse(200, fmt.Sprintf("Added torrent %s", *torrent.Name))
		w.WriteHeader(response.Code)
		w.Write(JSON)

		// f is one of the files
	}

	// file, header, err := r.FormFile("uploadfile")
	// if err != nil {
	// 	response, JSON := craftResponse(500, err.Error())
	// 	w.WriteHeader(response.Code)
	// 	w.Write(JSON)
	// 	return
	// }

}

func craftResponse(code int, msg string) (Response, []byte) {
	response := Response{
		Code:    code,
		Message: msg,
		Time:    time.Now().Unix(),
	}

	JSON, err := json.Marshal(response)
	if err != nil {
		return response, []byte(msg)
	}

	return response, JSON
}
