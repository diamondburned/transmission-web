package main

import (
	"bytes"
	"context"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"time"
)

var (
	cfg         = parseCfg()
	conn        = cfg.Transmission.initConnection()
	indexTpl    *template.Template
	statsTpl    *template.Template
	torrentsTpl *template.Template
)

// HTMLServer represents the web service that serves up HTML
type HTMLServer struct {
	server *http.Server
	wg     sync.WaitGroup
}

func readFile(file string) string {
	bytes, err := ioutil.ReadFile(file)
	if err != nil {
		log.Panicln(err)
	}

	return string(bytes)
}

func init() {
	var (
		index    = string(readFile("public/index.html"))
		stats    = string(readFile("public/stats.html"))
		torrents = string(readFile("public/torrents.html"))
	)

	indexTpl = template.Must(template.New("indexTpl").Parse(index))
	statsTpl = template.Must(template.New("statsTpl").Parse(stats))
	torrentsTpl = template.Must(template.New("torrentsTpl").Parse(torrents))
}

func main() {
	htmlServer := cfg.Start()
	defer htmlServer.Stop()

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	<-sigChan

	log.Println("main : shutting down")
}

// Start launches the HTML Server
func (cfg *Config) Start() *HTMLServer {
	// Setup Context
	_, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Setup Handlers
	http.HandleFunc("/", HomeHandler)
	http.HandleFunc("/torrents", LoadTorrents)
	http.HandleFunc("/stats", LoadStats)
	http.HandleFunc("/upload", UploadTorrent)
	router.PathPrefix("/api").HandlerFunc(APIHandler)
	router.PathPrefix("/static/").Handler(
		http.StripPrefix(
			"/static/",
			http.FileServer(http.Dir("./public/static")),
		),
	)

	// Create the HTML Server
	htmlServer := HTMLServer{
		server: &http.Server{
			Addr:           cfg.Host,
			Handler:        router,
			ReadTimeout:    cfg.ReadTimeout,
			WriteTimeout:   cfg.WriteTimeout,
			MaxHeaderBytes: 1 << 20,
		},
	}

	// Add to the WaitGroup for the listener goroutine
	htmlServer.wg.Add(1)

	// Start the listener
	go func() {
		log.Printf("\nHTMLServer : Service started : Host=%v\n", cfg.Host)
		htmlServer.server.ListenAndServe()
		htmlServer.wg.Done()
	}()

	return &htmlServer
}

// Stop turns off the HTML Server
func (htmlServer *HTMLServer) Stop() error {
	// Create a context to attempt a graceful 5 second shutdown.
	const timeout = 5 * time.Second
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	log.Printf("\nHTMLServer : Service stopping\n")

	// Attempt the graceful shutdown by closing the listener
	// and completing all inflight requests
	if err := htmlServer.server.Shutdown(ctx); err != nil {
		// Looks like we timed out on the graceful shutdown. Force close.
		if err := htmlServer.server.Close(); err != nil {
			log.Printf("\nHTMLServer : Service stopping : Error=%v\n", err)
			return err
		}
	}

	// Wait for the listener to report that it is closed.
	htmlServer.wg.Wait()
	log.Printf("\nHTMLServer : Stopped\n")
	return nil
}

// Push the given resource to the client.
func push(w http.ResponseWriter, resource string) {
	pusher, ok := w.(http.Pusher)
	if ok {
		if err := pusher.Push(resource, nil); err == nil {
			return
		}
	}
}

// Render a template, or server error.
func render(w http.ResponseWriter, r *http.Request, tpl *template.Template, name string, data interface{}) {
	buf := new(bytes.Buffer)
	if err := tpl.ExecuteTemplate(buf, name, data); err != nil {
		fmt.Printf("\nRender Error: %v\n", err)
		return
	}
	w.Write(buf.Bytes())
}
