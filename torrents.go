package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"sort"
	"strings"
	"sync"
	"time"

	humanize "github.com/dustin/go-humanize"
	"github.com/odwrtw/transmission"
)

/*
	Status code for Transmission is kind of stupid.

	Just keep in mind that:
		- 0 is Paused
		- 4 is Downloading
		- 6 is Seeding
*/

type contextKey uint8

const (
	stateContextKey contextKey = iota
)

// StateUpdateFreq is the maximum frequency the state is allowed to update.
const StateUpdateFreq = 2 * time.Second

type State struct {
	*transmission.Client

	mutex       sync.Mutex
	updating    chan struct{}
	lastUpdated time.Time

	stats    transmission.Statistics
	torrents []*transmission.Torrent
}

// NewState creates a new Transmission state.
func NewState(client *transmission.Client) *State {
	return &State{
		Client:   client,
		updating: make(chan struct{}, 1),
	}
}

// ContextState gets the stored Transmission state from context.
func ContextState(ctx context.Context) *State {
	v := ctx.Value(stateContextKey)
	if v == nil {
		return nil
	}

	// Hijack and ask for an update asynchronously.
	state := v.(*State)
	state.AskUpdate(true)

	return state
}

// RequestState gets the stored Transmission state from the request.
func RequestState(r *http.Request) *State {
	return ContextState(r.Context())
}

func (s *State) InjectHTTP(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		next.ServeHTTP(w, r.WithContext(
			context.WithValue(r.Context(), stateContextKey, s),
		))
	})
}

func (s *State) FindTorrents(ids ...int) []*transmission.Torrent {
	var finds = make([]*transmission.Torrent, 0, len(ids))

	s.mutex.Lock()
	defer s.mutex.Unlock()

	for _, id := range ids {

		for _, t := range s.torrents {
			if t.ID == id {
				cpy := *t
				finds = append(finds, &cpy)
				break
			}
		}

	}

	return finds
}

func (s *State) LastUpdated() time.Time {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	return s.lastUpdated
}

// AskUpdate asks the state to be updated. This function blocks if it updates.
func (s *State) AskUpdate(async bool) bool {
	var now = time.Now()

	// If we're updating too quickly, then skip.
	if now.Add(-StateUpdateFreq).Before(s.LastUpdated()) {
		return false
	}

	select {
	case s.updating <- struct{}{}:
		if async {
			go s.updateAndRelease(now)
		} else {
			s.updateAndRelease(now)
		}

		return true
	default:
		return false
	}
}

func (s *State) updateAndRelease(now time.Time) {
	s.update(now)
	<-s.updating
}

func (s *State) update(now time.Time) {
	stats, statsErr := s.Client.Session.Stats()
	log.Println("Updating stats; error:", statsErr)

	torrents, torrentsErr := s.Client.GetTorrents()
	log.Println("Updated torrents; error:", torrentsErr)

	s.mutex.Lock()
	defer s.mutex.Unlock()

	s.lastUpdated = now

	if statsErr == nil {
		s.stats = stats
	}
	if torrentsErr == nil {
		s.torrents = torrents
	}
}

// HTMLTorrent is the structure for each torrent entry
type HTMLTorrent struct {
	Error         string
	ID            string
	Status        int
	Name          string
	Size          string // when done
	Progress      string
	DateAdded     string
	Ratio         string
	Uploaded      string
	UploadSpeed   string
	DownloadSpeed string

	// used for sorting
	torrent *transmission.Torrent
}

// HTMLStats is the structure for the sidebar
type HTMLStats struct {
	ActiveTorrentCount string
	PausedTorrentCount string
	TotalTorrentCount  string
	DownloadSpeed      string
	UploadSpeed        string
	CumDownload        string
	CumUpload          string
	CurDownload        string
	CurUpload          string
	DownloadSpeedBytes uint64
	UploadSpeedBytes   uint64
	LastUpdated        time.Time
}

func (s *State) HTMLTorrents(mode SortMode, reversed bool) []HTMLTorrent {
	s.mutex.Lock()

	var htmlTorrents = make([]HTMLTorrent, len(s.torrents))
	for i, t := range s.torrents {
		htmlTorrents[i] = HTMLTorrent{
			Error:         strings.TrimSpace(t.ErrorString),
			ID:            fmt.Sprintf("%v", t.ID),
			Status:        int(t.Status),
			Name:          t.Name,
			Size:          humanize.Bytes(uint64(t.SizeWhenDone)),
			Progress:      fmt.Sprintf("%.5f%%", t.PercentDone*100),
			DateAdded:     time.Unix(int64(t.DateCreated), 0).Format("2006/01/02 15:04"),
			Ratio:         fmt.Sprintf("%.2f", t.UploadRatio),
			Uploaded:      humanize.Bytes(uint64(t.UploadedEver)),
			UploadSpeed:   humanize.Bytes(uint64(t.RateUpload)),
			DownloadSpeed: humanize.Bytes(uint64(t.RateDownload)),

			// A copy of the torrent pointer for sorting.
			torrent: s.torrents[i],
		}
	}

	s.mutex.Unlock()

	var sortable sort.Interface = TorrentsSortable{
		Torrents: htmlTorrents,
		Mode:     mode,
	}

	if reversed {
		sortable = sort.Reverse(sortable)
	}

	sort.Sort(sortable)

	return htmlTorrents
}

func (s *State) HTMLStats() HTMLStats {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	if s.stats.CumulativeStats == nil || s.stats.CurrentStats == nil {
		return HTMLStats{}
	}

	return HTMLStats{
		ActiveTorrentCount: humanize.Comma(int64(s.stats.ActiveTorrentCount)),
		PausedTorrentCount: humanize.Comma(int64(s.stats.PausedTorrentCount)),
		TotalTorrentCount:  humanize.Comma(int64(s.stats.TorrentCount)),
		DownloadSpeed:      humanize.Bytes(uint64(s.stats.DownloadSpeed)) + "/s",
		UploadSpeed:        humanize.Bytes(uint64(s.stats.UploadSpeed)) + "/s",
		CumDownload:        humanize.Bytes(uint64(s.stats.CumulativeStats.DownloadedBytes)),
		CumUpload:          humanize.Bytes(uint64(s.stats.CumulativeStats.UploadedBytes)),
		CurDownload:        humanize.Bytes(uint64(s.stats.CurrentStats.DownloadedBytes)),
		CurUpload:          humanize.Bytes(uint64(s.stats.CurrentStats.UploadedBytes)),
		DownloadSpeedBytes: uint64(s.stats.DownloadSpeed),
		UploadSpeedBytes:   uint64(s.stats.UploadSpeed),
		LastUpdated:        s.lastUpdated,
	}
}

func SecondsTime(seconds int) time.Time {
	return time.Unix(int64(seconds), 0)
}

func PeersSum(peersFrom transmission.PeersFrom) int {
	return 0 +
		peersFrom.FromTracker +
		peersFrom.FromPex +
		peersFrom.FromIncoming +
		peersFrom.FromCache
}
