package main

import (
	"archive/tar"
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/go-chi/chi"
	"github.com/odwrtw/transmission"
	"github.com/pkg/errors"
)

var (
	// ErrInvalidID is returned when the API receives some malformed form
	ErrInvalidID = errors.New("Missing ID. ID should look like: '1,2,3,5'")
)

// Response ..
type Response struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
	Time    int64  `json:"epoch_time"`
}

// TorrentInfo ..
type TorrentInfo struct {
	ActivityDate string
	AddedDate    string
	Comment      string
	Peers        string
	DownloadDir  string
	Trackers     string
	Time         int64
}

// HomeHandler renders the homepage view template
func HomeHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	render(w, r, indexTpl, "indexTpl", nil)
}

// StaticHandler handles static assets eg js/css
func StaticHandler(w http.ResponseWriter, r *http.Request) {
	res, err := public.Find(r.URL.Path)
	if err != nil {
		w.WriteHeader(404)
		w.Write([]byte(err.Error()))
		return
	}

	var contentType string

	switch path.Ext(r.URL.Path) {
	case ".js":
		contentType = "application/javascript"
	case ".css":
		contentType = "text/css"
	case ".html", ".htm":
		contentType = "text/html"
	default:
		var maxLen = 512
		if maxLen > len(res) {
			maxLen = len(res)
		}
		contentType = http.DetectContentType(res[:maxLen])
	}

	// Set the appropriate header
	w.Header().Set("Content-Type", contentType)

	w.WriteHeader(200)
	w.Write(res)
}

// LoadTorrents is separated for AJAX purposes
// Contains a list of torrents and stuff
func LoadTorrents(w http.ResponseWriter, r *http.Request) {
	sortmode := AddedDate
	switch r.URL.Query().Get("sort") {
	case "ID":
		sortmode = ID
	case "AddedDate":
		sortmode = AddedDate
	case "ActivityDate":
		sortmode = ActivityDate
	case "DoneDate":
		sortmode = DoneDate
	case "Name":
		sortmode = Name
	case "Size":
		sortmode = Size
	case "Ratio":
		sortmode = Ratio
	case "UploadSpeed":
		sortmode = UploadSpeed
	case "DownloadSpeed":
		sortmode = DownloadSpeed
	}

	reversed := false
	switch r.URL.Query().Get("reversed") {
	case "1":
		reversed = true
	}

	fullData := map[string]interface{}{
		"Torrents": RequestState(r).HTMLTorrents(sortmode, reversed),
	}

	render(w, r, torrentsTpl, "torrentsTpl", fullData)
}

// LoadStats is separated for AJAX purposes
// Contains stats for download speed, upload speed, etc
func LoadStats(w http.ResponseWriter, r *http.Request) {
	render(w, r, statsTpl, "statsTpl", map[string]interface{}{
		"Stats": RequestState(r).HTMLStats(),
	})
}

// APIHandler does API stuff
func APIHandler(w http.ResponseWriter, r *http.Request) {
	defer func() {
		if r := recover(); r != nil {
			http.Error(w, fmt.Sprintf("%v", r), 500)
		}
	}()

	var state = RequestState(r)

	switch route := chi.URLParam(r, "command"); route {
	// /api/pause and /api/continue are the endpoint for pausing torrent(s)
	// Usage: Send a form request to /api/[pause|continue], name "ids", torrent IDs separated by a
	// comma.
	// Example: "23,10,6,3"
	case "pause", "continue":
		if r.Method != "POST" {
			response, JSON := craftResponse(400, "Endpoint only accepts POST request.")
			w.WriteHeader(response.Code)
			w.Write(JSON)
			return
		}

		IDs, err := splitIDfromForm(r)
		if err != nil {
			response, JSON := craftResponse(500, err.Error())
			w.WriteHeader(response.Code)
			w.Write(JSON)
			return
		}

		torrents := state.FindTorrents(IDs...)
		if len(torrents) != len(IDs) {
			http.Error(w, fmt.Sprintf("Torrent(s) not found %v", IDs), 500)
			return
		}

		for _, torrent := range torrents {
			switch route {
			case "pause":
				err = torrent.Stop()
			case "continue":
				err = torrent.StartNow()
			}

			if err != nil {
				err = errors.Wrapf(err, "Failed to stop torrent %d", torrent.ID)
				break
			}
		}

		var resp string
		var code int

		if err != nil {
			resp = err.Error()
			code = 500
		} else {
			resp = "Successfully set for torrents " + r.FormValue("ids")
			code = 200
		}

		response, JSON := craftResponse(code, resp)
		w.WriteHeader(response.Code)
		w.Write(JSON)

	case "delete", "remove":
		IDs, err := splitIDfromForm(r)
		if err != nil {
			http.Error(w, err.Error(), 400)
			return
		}

		torrents := state.FindTorrents(IDs...)

		if err := state.RemoveTorrents(torrents, route == "delete"); err != nil {
			http.Error(w, err.Error(), 400)
			return
		}

		writeResponse(w, 200,
			"Successfully deleted/removed torrents "+r.FormValue("ids"))

	case "torrent":
		IDs, err := splitIDfromForm(r)
		if err != nil {
			http.Error(w, err.Error(), 400)
			return
		}

		torrents := state.FindTorrents(IDs[0])
		if len(torrents) != 1 {
			http.Error(w, "Torrent not found", 500)
			return
		}

		var torrent = torrents[0]
		var peersSum int
		var trackersSum int

		if torrent.Peers != nil {
			peersSum = len(*torrent.Peers)
		} else {
			peersSum = PeersSum(torrent.PeersFrom)
		}

		switch {
		case torrent.Trackers != nil:
			trackersSum = len(*torrent.Trackers)
		case torrent.TrackerStats != nil:
			trackersSum = len(*torrent.TrackerStats)
		}

		torrentInfo := TorrentInfo{
			ActivityDate: SecondsTime(torrent.ActivityDate).Format(time.RFC1123Z),
			AddedDate:    SecondsTime(torrent.AddedDate).Format(time.RFC1123Z),
			Comment:      torrent.Comment,
			Peers:        fmt.Sprintf("%d / %d", torrent.PeersConnected, peersSum),
			Trackers:     strconv.Itoa(trackersSum),
			DownloadDir:  torrent.DownloadDir,
			Time:         time.Now().Unix(),
		}

		bytes, err := json.Marshal(torrentInfo)
		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}

		w.WriteHeader(200)
		w.Write(bytes)

	case "tar":
		IDs, err := splitIDfromForm(r)
		if err != nil {
			http.Error(w, err.Error(), 400)
			return
		}

		torrents := state.FindTorrents(IDs[0])
		if len(torrents) != 1 {
			http.Error(w, "Torrent not found", 500)
			return
		}

		var torrent = torrents[0]
		var destination = filepath.Join(torrent.DownloadDir, torrent.Name)

		if _, err := os.Stat(destination); err != nil {
			http.Error(w, "Stat failed: "+err.Error(), 500)
			return
		}

		var base = filepath.Base(destination)

		w.Header().Set(
			"Content-Disposition",
			"attachment; filename="+url.PathEscape(base)+".tar",
		)

		tw := tar.NewWriter(w)
		defer tw.Close()

		if err := filepath.Walk(destination,
			func(p string, i os.FileInfo, err error) error {
				if err != nil { // ?? :D ??
					return err
				}

				if !i.Mode().IsRegular() {
					return nil
				}

				h, err := tar.FileInfoHeader(i, i.Name())
				if err != nil {
					return err
				}

				rel, err := filepath.Rel(destination, p)
				if err != nil {
					return err
				}

				h.Name = rel

				if err := tw.WriteHeader(h); err != nil {
					return err
				}

				f, err := os.Open(p)
				if err != nil {
					return err
				}

				defer f.Close()

				if _, err := io.Copy(tw, f); err != nil {
					return err
				}

				return nil
			},
		); err != nil {
			log.Println(err.Error())
			http.Error(w, err.Error(), 500)
		}
	default:
		response, JSON := craftResponse(404, "API path not found")
		w.WriteHeader(response.Code)
		w.Write(JSON)
	}
}

func splitIDfromForm(r *http.Request) (IDs []int, err error) {
	form := r.FormValue("id")
	if form == "" {
		err = ErrInvalidID
		return
	}

	ids := strings.Split(form, ",")
	IDs = make([]int, 0, len(ids))

	for _, id := range ids {
		ID, err := strconv.Atoi(id)
		if err != nil {
			return IDs, err
		}

		IDs = append(IDs, ID)
	}

	if len(IDs) == 0 {
		err = errors.New("No IDs")
	}

	return
}

// UploadTorrent listens to a XHRHTTPRequest containing a torrent binary and adds it
func UploadTorrent(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		response, JSON := craftResponse(400, "Upload only accepts POST request.")
		w.WriteHeader(response.Code)
		w.Write(JSON)
		return
	}

	if err := r.ParseMultipartForm(32 << 10); err != nil {
		writeResponse(w, 400, err.Error())
		return
	}

	state := RequestState(r)

	headers, ok := r.MultipartForm.File["uploadfile"]
	if !ok || headers == nil {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			writeResponse(w, 500, err.Error())
			return
		}

		var magnet = string(body)

		if !strings.HasPrefix(magnet, "magnet:") {
			writeResponse(w, 400, "Not a magnet link")
			return
		}

		torrent, err := state.AddTorrent(transmission.AddTorrentArg{
			Filename: magnet,
		})
		if err != nil {
			writeResponse(w, 500, err.Error())
			return
		}

		writeResponse(w, 200, fmt.Sprintf("Added torrent %s", torrent.Name))
		return
	}

	if len(headers) == 0 {
		response, JSON := craftResponse(400, "No files in form 'uploadfile'")
		w.WriteHeader(response.Code)
		w.Write(JSON)
		return
	}

	// If the request does indeed contain files

	var buf bytes.Buffer
	var msg = make([]string, 0, len(headers))

	for _, header := range headers {
		file, err := header.Open()
		if err != nil {
			writeResponse(w, 500, "Failed to open torrent file: "+err.Error())
			return
		}

		log.Println("Received file:", header.Filename, "/ size:", header.Size)

		if _, err := buf.ReadFrom(file); err != nil {
			file.Close()

			writeResponse(w, 500, err.Error())
			return
		}

		file.Close()

		if buf.Len() < 2 {
			writeResponse(w, 400, "Missing data-binary as a torrent file.")
			return
		}

		torrent, err := state.AddTorrent(transmission.AddTorrentArg{
			Metainfo: base64.StdEncoding.EncodeToString(buf.Bytes()),
		})
		if err != nil {
			writeResponse(w, 500, err.Error())
			return
		}

		msg = append(msg, fmt.Sprintf("Added torrent %s", torrent.Name))
		buf.Reset()
	}

	writeResponse(w, 200, strings.Join(msg, "\n"))
}

func writeResponse(w http.ResponseWriter, code int, msg string) {
	resp, JSON := craftResponse(code, msg)
	w.WriteHeader(resp.Code)
	w.Write(JSON)
}

func craftResponse(code int, msg string) (Response, []byte) {
	response := Response{
		Code:    code,
		Message: msg,
		Time:    time.Now().Unix(),
	}

	JSON, err := json.Marshal(response)
	if err != nil {
		return response, []byte(msg)
	}

	return response, JSON
}
